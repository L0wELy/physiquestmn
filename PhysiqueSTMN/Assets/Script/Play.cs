﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Play : MonoBehaviour {

    [SerializeField]
    Text m_playText;

    [SerializeField]
    List<Physics> physics;

    bool launched = false;


    //Lance ou arrete la simulation.
    public void Launch()
    {
        launched = !launched;

        foreach(Physics i in physics)
        {
            i.Init();
            i.playPhysics = launched;
        }

        if (launched)
            m_playText.text = "Stop !";
        else
            m_playText.text = "Jouer !";

    }

}
