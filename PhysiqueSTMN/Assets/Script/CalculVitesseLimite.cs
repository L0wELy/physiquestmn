﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class CalculVitesseLimite : MonoBehaviour {


    [SerializeField]
    List<Physics> physics;

    [SerializeField]
    List<Text> resultatCalculs;

    [SerializeField]
    float deltaLimite = 0.0001f;


    public void Calcul()
    {
        int cpt = 0;

        foreach (Physics i in physics)
        {
            float resultat = 0;
            float calcul = 0;

            bool wrongCalcul = false;

            float lastResult = 0;

            while ((Mathf.Abs((resultat = i.Formule(calcul,i.gravity.y))) > deltaLimite))
            {

                //Il est incohérent d'avoir une vitesse qui augmente/diminue au cours du temps.
                if (lastResult != 0 && Mathf.Sign(lastResult) != Mathf.Sign(resultat))
                {
                    wrongCalcul = true;
                    break;
                }

                calcul += resultat;
                lastResult = resultat;
            }

            if (!wrongCalcul)
                resultatCalculs[cpt].text = "Vitesse limite pour l'objet : " + calcul + " unité(s) Unity / s";
            else
            {
                resultatCalculs[cpt].text = "Vos paramètres n'ont pas permis de déterminer la vitesse limite : vitesse infinie/incohérente";
            }

            cpt++;
        }
    }
}
