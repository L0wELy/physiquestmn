﻿using UnityEngine;
using System.Collections;

public class Physics : MonoBehaviour {

    public float coefficientDeFrottement;
    public Vector2 gravity;
    public float masseDeLairDeplace;
    public float masseDeLobjet;

    public Vector2 m_speed;


    public bool playPhysics = false;

    [SerializeField]
    Transform m_transform;


    Vector2 m_initialPosition;
    public Vector2 m_initialSpeed;

	void Start () {
        m_initialPosition = m_transform.position;
        m_initialSpeed = m_speed;
	}


	void FixedUpdate () {

        //Si la physique n'est pas lancée, on retourne directement
        if (!playPhysics)
            return;

        //Formule d'un objet soumis à la gravité, aux forces de frottement de l'air et à la poussée d'Archimède provoqué par l'air.
        m_speed.y += Formule(m_speed.y,gravity.y);

        //Si jamais vous voulez faire mumuse avec la gravité en x
        m_speed.x += Formule(m_speed.x, gravity.x);

        //On applique le mouvement par translation de l'objet.
        m_transform.Translate(m_speed);


        //JE fais ça sinon on peut partir très loin avec des coefficients mal définis.
        if (Mathf.Abs(m_transform.position.y) > 100)
            Init();
	}

    //Formule pour la vitesse en y d'un objet en chute libre dans l'air.
    public float Formule(float currentSpeed, float gravity)
    {
        return -coefficientDeFrottement / masseDeLobjet * currentSpeed + (masseDeLairDeplace / masseDeLobjet - 1) * gravity;
    }


    public void Init()
    {
        m_transform.position = m_initialPosition;
        m_speed = m_initialSpeed;
    }
}
