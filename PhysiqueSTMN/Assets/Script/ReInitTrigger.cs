﻿using UnityEngine;
using System.Collections;

public class ReInitTrigger : MonoBehaviour {

    [SerializeField]
    Transform m_tp;




    void OnTriggerEnter2D(Collider2D other)
    {
        Physics p = other.GetComponent<Physics>();

        p.m_speed = new Vector2(0, 0);
        p.transform.position = new Vector2(p.transform.position.x, m_tp.position.y);
    }
}
