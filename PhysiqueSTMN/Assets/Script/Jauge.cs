﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Jauge : MonoBehaviour {

    [SerializeField]
    Physics physics;

    [SerializeField]
    Text gravityText;

    [SerializeField]
    Text coefficientDeFrottementText;

    [SerializeField]
    Text masseObjetText;

    [SerializeField]
    Text masseAirText;

    [SerializeField]
    Text vitesseText;

    [SerializeField]
    Text angleText;


    float angle;
    float force;

    void Start()
    {
        angle = Mathf.Atan2(physics.m_initialSpeed.y, physics.m_initialSpeed.x);
        force = physics.m_initialSpeed.magnitude;

        gravityText.text = "Gravité : " + physics.gravity.y;
        coefficientDeFrottementText.text = "Coefficient de frottement de l'air : " + physics.coefficientDeFrottement;
        masseObjetText.text = "Masse de l'objet : " + physics.masseDeLobjet;
        masseAirText.text = "Masse de l'air déplacé : " + physics.masseDeLairDeplace;

        if (!vitesseText == null)
        { 
            vitesseText.text = "Vitesse initiale : " + physics.m_initialSpeed.magnitude;
            angleText.text = "Angle : " + Mathf.Atan2(physics.m_initialSpeed.y, physics.m_initialSpeed.x);
        }
 
    }

    public void ChangeYGravity(float g)
    {
        physics.gravity.y = g;
        gravityText.text = "Gravité : " + g;
    }

    public void ChangeCoefficientFrottement(float lambda)
    {
        physics.coefficientDeFrottement = lambda;
        coefficientDeFrottementText.text = "Coefficient de frottement de l'air : " + lambda;
    }

    public void ChangeMasseObjet(float m)
    {
        physics.masseDeLobjet = m;
        masseObjetText.text = "Masse de l'objet : " + m;
    }

    public void ChangeMasseAir(float m)
    {
        physics.masseDeLairDeplace = m;
        masseAirText.text = "Masse de l'air déplacé : " + m;
    }

    public void ChangeMagnitude(float m)
    {
        force = m;

        Vector2 newVector = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;
        physics.m_initialSpeed = new Vector2(newVector.x*m, newVector.y*m);
        vitesseText.text = "Vitesse initiale : " + m;
    }

    public void ChangeAngle(float m)
    {
        angle = m;

        physics.m_initialSpeed = new Vector2(Mathf.Cos(m) * force, Mathf.Sin(m) * force);
        angleText.text = "Angle : " + m;
    }
}
